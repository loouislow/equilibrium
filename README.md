# equilibrium
> Time machine backup solution for Linux, Mac

I wrote this because my working place has a terribly bad IT culture. 'equilibrium' helps me incrementally backup changes on my primary drive to other drives. You can put it on cronjob as well.

`backup.sh` is an init script.

`equilibrium.sh` is a main application script.

create `backup.marker` file at destination backup folder or drive.

> **Note** I'll write a complete README when I am free.
