#!/bin/bash
#
# @@script: backup.sh
# @@description: incremental backup init script
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### constant
export USER="loouis"
export HOME="/media/loouis/Storage"

BACKUP_DIR="${HOME}/backup"
DEST_DIR="user-loouis"
MEDIUM="/media/loouis/Storage"

BACKUP_SOURCE="/home/loouis"
BACKUP_DESTINATION="${MEDIUM}/backup/usr-loouis"
BACKUP_SIZE="/dev/sda"

FLTR_FOLDER_1="Downloads"
FLTR_FOLDER_2="Musics"
FLTR_FOLDER_3="Videos"
FLTR_FOLDER_4="Pictures"
FLTR_FOLDER_5="log"
FLTR_FOLDER_5="Public"

LOG="/home/loouis/.log/backup.log"

### runas
function root_only () {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo -e "\e[34m[backup]\e[39m Permission denied."
         exit 1
   fi
}

### run equilibrium
function run_backup () {
   echo -e "\e[34m[backup]\e[39m started..."
   equilibrium ${BACKUP_SOURCE} ${BACKUP_DESTINATION}
   echo -e "\e[34m[backup]\e[39m done!"
}

### remove unwanted folders
function optimize_backup () {
   echo -e "\e[34m[backup]\e[39m optimizing backup..."

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_1} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: Downloads removed!"

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_2} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: Musics removed!"

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_3} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: Videos removed!"

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_4} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: Pictures removed!"

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_5} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: log removed!"

   find ${BACKUP_DIR}/{$DEST_DIR} -name ${FLTR_FOLDER_6} -type d -exec rm -r {} \;
   echo -e "\e[34m[backup]\e[39m FOLDER: Public removed!"
}

### display total backup size
function show_backup_size () {
   echo -e "\e[34m[backup]\e[39m getting backup size..."
   df ${BACKUP_SIZE}
}

### write log
function write_log () {
   echo -e "\e[34m[backup]\e[39m sync finished at" `date` >> ${LOG}
}

### init
root_only
run_backup
optimize_backup
write_log
show_backup_size
